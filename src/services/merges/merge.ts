export interface Merge {
    cod: number;
    issueCod: number;
    state: string;
    mergedby: string;
    mergedat: Date;
    targetbranch: string;
    milestone: string;
    link: string;
}