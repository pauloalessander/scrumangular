import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Merge } from './merge';
import { DevMerged } from './devmerged';

@Injectable({
  providedIn: 'root'
})
export class MergesService {

  //private mergesUrl = 'assets/merges.json';
  private mergesUrl = 'http://localhost:8000/api/merge/opened?milestone=';

  private mergesByDevs = 'http://localhost:8000/api/merge/quantmerged?milestone=';

  private update = 'http://localhost:8000/api/merge/update?milestone=';
  
  constructor(private http: HttpClient) { }
  
  //import and refresh data
  importRefreshMilestone(milestone: string) {
    const uri = this.update + milestone + '&project=47'
    return this.http.get<any>(uri).pipe(
      map((response) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }
  
  // merges list
  getMerges(offset: number, pageSize: number, sortField: string, sortDirection: string, milestone: string): Observable<Merge[]> {
    const uri = this.mergesUrl + milestone;
    return this.http.get<Merge[]>(uri).pipe(
      map((response) => {
        return this.getPagedData(
          this.getSortedData(
            response,
            sortField,
            sortDirection),
          offset, pageSize);
      }),
      catchError(this.handleError)
    );
  }

  getMergeCount(milestone: string): Observable<number> {
    const uri = this.mergesUrl + milestone;
    return this.http.get<Merge[]>(uri).pipe(
      map((response) => {
        return response.length;
      }),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `Um erro ocorreu: ${err.error.message}`;
    } else {
      errorMessage = `Código servidor: ${err.status}, mensagem de erro: ${err.message}`;
    }

    return throwError(errorMessage);
  }

  private getPagedData(data: Merge[], startIndex: number, pageSize: number) {
    return data.splice(startIndex, pageSize);
  }

  private getSortedData(data: Merge[], active: string, direction: string) {
    if (!active || direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = direction === 'asc';
      switch (active) {
        case "cod": return compare(+a.cod, +b.cod, isAsc);
        case "issueCod": return compare(+a.issueCod, +b.issueCod, isAsc);
        case "mergedby": return compare(a.mergedby, b.mergedby, isAsc);
        case "mergedat": return compare(a.mergedat, b.mergedat, isAsc);
        case "targetbranch": return compare(a.targetbranch, b.targetbranch, isAsc);
        case "milestone": return compare(a.milestone, b.milestone, isAsc);
        case "link": return compare(a.link, b.link, isAsc);
        case "state": return compare(a.state, b.state, isAsc);
        default: return 0;
      }
    });
  }

  // quant merged
  getMerged(milestone: string): Observable<DevMerged[]> {
    const uri = this.mergesByDevs + milestone;
    return this.http.get<DevMerged[]>(uri).pipe(
      map(response => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getSponsorFromDevMerged(merges: DevMerged[]): string[] {
    const sponsor: string[] = [];
    merges.forEach(element => {
      sponsor.push(element.sponsor)
    });
    return sponsor;
  }

  getQuantMergesFromDevMerged(merges: DevMerged[]): number[] {
    const quantMerges: number[] = [];
    merges.forEach(element => {
      quantMerges.push(element.quant_merges)
    });
    return quantMerges;
  }
}

function compare(a: string | number | Date, b: string | number | Date, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
