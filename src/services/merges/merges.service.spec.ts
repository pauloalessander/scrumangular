import { TestBed } from '@angular/core/testing';

import { MergesService } from './merges.service';

describe('MergesService', () => {
  let service: MergesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MergesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
