import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, tap, throwError } from 'rxjs';
import { Day } from './day';
import { Issue } from './issue';
import { Quant } from './quant';
import { Status } from './status';
import { Sums } from './sum';

@Injectable({
  providedIn: 'root'
})
export class IssuesService {

  private issuesUrl = 'http://localhost:8000/api/issue?milestone=';

  private quant = 'http://localhost:8000/api/issue/quantissues?milestone=';

  private sums = 'http://localhost:8000/api/issue/sumissues?milestone=';

  private days = 'http://localhost:8000/api/issue/dayissues?milestone=';

  private status = 'http://localhost:8000/api/issue/statusissues?milestone=';

  private update = 'http://localhost:8000/api/issue/update?milestone=';

  //private issuesUrl = 'assets/issues.json';

  constructor(private http: HttpClient) { }

  //import and refresh data
  importRefreshMilestone(milestone: string) {
    const uri = this.update + milestone + '&project=47'
    return this.http.get<any>(uri).pipe(
      map((response) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  //issues list
  getIssues(offset: number, pageSize: number, sortField: string, sortDirection: string, milestone: string): Observable<Issue[]> {
    const uri = this.issuesUrl + milestone;
    return this.http.get<Issue[]>(uri).pipe(
      map((response) => {
        return this.getPagedData(
          this.getSortedData(
            response,
            sortField,
            sortDirection),
          offset, pageSize);
      }),
      catchError(this.handleError)
    );
  }

  getIssueCount(milestone: string): Observable<number> {
    const uri = this.issuesUrl + milestone;
    return this.http.get<Issue[]>(uri).pipe(
      map((response) => {
        return response.length;
      }),
      catchError(this.handleError)
    );
  }
  
  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `Um erro ocorreu: ${err.error.message}`;
    } else {
      errorMessage = `Código servidor: ${err.status}, mensagem de erro: ${err.message}`;
    }

    return throwError(errorMessage);
  }

  private getPagedData(data: Issue[], startIndex: number, pageSize: number) {
    return data.splice(startIndex, pageSize);
  }

  private getSortedData(data: Issue[], active: string, direction: string) {
    if (!active || direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = direction === 'asc';
      switch (active) {
        case 'cod': return compare(a.cod, b.cod, isAsc);
        case 'title': return compare(a.title, b.title, isAsc);
        case 'createdat': return compare(a.createdat, b.createdat, isAsc);
        case 'author': return compare(a.author, b.author, isAsc);
        case 'sponsor': return compare(a.sponsor, b.sponsor, isAsc);
        case 'link': return compare(a.link, b.link, isAsc);
        case 'milestone': return compare(a.milestone, b.milestone, isAsc);
        case 'time': return compare(a.time, b.time, isAsc);
        case 'timedesc': return compare(a.timedesc, b.timedesc, isAsc);
        case 'planning': return compare(a.planning, b.planning, isAsc);
        case 'status': return compare(a.status, b.status, isAsc);
        case 'project': return compare(a.project, b.project, isAsc);
        case 'point': return compare(a.point, b.point, isAsc);
        case 'isplan': return compare(a.isplan, b.isplan, isAsc);
        default: return 0;
      }
    });
  }

  //quant issues
  getQuant(milestone: string): Observable<Quant[]> {
    const uri = this.quant + milestone;
    return this.http.get<Quant[]>(uri).pipe(
      map((response) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getQuantIssuesFromQuant(quant: Quant[]): number[] {
    const quantIssues: number[] = [];
    quant.forEach(element => {
      quantIssues.push(element.quant_issues);
    }
    );
    return quantIssues;
  }

  getSponsorsFromQuant(quant: Quant[]): string[] {
    const sponsors: string[] = [];
    quant.forEach(element => {
      sponsors.push(element.sponsor);
    }
    );
    return sponsors;
  }

  //sum issues
  getSums(milestone: string): Observable<Sums[]> {
    const uri = this.sums + milestone;
    return this.http.get<Sums[]>(uri).pipe(
      map((response) => {
        return response;
      }
      ),
      catchError(this.handleError)
    );
  }

  getSumIssuesFromSums(sum: Sums[]): number[] {
    const sumIssues: number[] = [];
    sum.forEach(element => {
      sumIssues.push(element.sum_points);
    }
    );
    return sumIssues;
  }

  getSponsorsFromSums(sum: Sums[]): string[] {
    const sponsors: string[] = [];
    sum.forEach(element => {
      sponsors.push(element.sponsor);
    }
    );
    return sponsors;
  }

  //day issues
  getDays(milestone: string): Observable<Day[]> {
    const uri = this.days + milestone;
    return this.http.get<Day[]>(uri).pipe(
      map((response) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getQuantIssuesFromDays(days: Day[]): number[] {
    const quantIssues: number[] = [];
    days.forEach(element => {
      quantIssues.push(element.quant_issues);
    }
    );
    return quantIssues;
  }

  getCreatedAtFromDays(days: Day[]): Date[] {
    const createdAts: Date[] = [];
    days.forEach(element => {
      createdAts.push(element.createdat);
    });
    return createdAts;
  }

  //status issues
  getStatus(milestone: string): Observable<Status[]> {
    const uri = this.status + milestone;
    return this.http.get<Status[]>(uri).pipe(
      map((response) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getQuantIssuesFromStatus(status: Status[]): number[] {
    const quantIssues: number[] = [];
    status.forEach(element => {
      quantIssues.push(element.quant_issues);
    }
    );
    return quantIssues;
  }

  getStatusFromStatus(statusObject: Status[]): string[] {
    const statusList: string[] = [];
    statusObject.forEach(element => {
      statusList.push(element.status);
    });
    return statusList;
  }

}
function compare(a: number | string | boolean | Date, b: number | string| boolean | Date, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}