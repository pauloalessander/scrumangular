export interface Issue {
    cod: number;
    title: string;
    createdat: Date;
    author: string;
    sponsor: string;
    link: string;
    milestone: string;
    time: number;
    timedesc: string;
    planning: string;
    status: string;
    project: string;
    point: number;
    isplan: boolean;
}