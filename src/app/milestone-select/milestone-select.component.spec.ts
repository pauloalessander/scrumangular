import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MilestoneSelectComponent } from './milestone-select.component';

describe('MilestoneSelectComponent', () => {
  let component: MilestoneSelectComponent;
  let fixture: ComponentFixture<MilestoneSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MilestoneSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
