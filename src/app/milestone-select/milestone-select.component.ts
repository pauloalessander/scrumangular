import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-milestone-select',
  templateUrl: './milestone-select.component.html',
  styleUrls: ['./milestone-select.component.css']
})
export class MilestoneSelectComponent implements OnInit {
  // Dados de exemplo
  milestones: any[] = [
    { name: 'Girigotango' },
    { name: 'sasageyo' },
    { name: 'Ouroboros' },
    { name: 'Disco de Menino' }
];

  constructor() { }

  ngOnInit(): void {
  }

}
