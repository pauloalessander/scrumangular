import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashTablesComponent } from './dash-tables/dash-tables.component';
import { DashComponent } from './dash/dash.component';

const routes: Routes = [
  { path: 'graphs', component: DashComponent },
  { path: 'tables', component: DashTablesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
