import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent {
  cardLayout = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(() => { 
     return {
        columns: 4,
        miniCard: { cols: 1, rows: 1 },
        table: { cols: 2, rows: 3 },
        chart: { cols: 4, rows: 3 },
        merges: { cols: 4, rows: 3 }
      };
    })
  );

  milestone!: string;

  constructor(private breakpointObserver: BreakpointObserver) {}

  receiveMilestone(value: string) {  
    this.milestone = value
  }

}
