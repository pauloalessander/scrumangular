import { Component, EventEmitter, Output } from '@angular/core';
import { IssuesService } from '../../services/issues/issues.service';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent {
  
  @Output() emmiter = new EventEmitter<string>();

  entry!: string;

  constructor(private issueService: IssuesService) {}

  onSubmit(value: string) {
    this.emmiter.emit(value);
  }

  importRefreshData(milestone: string) {
    this.issueService.importRefreshMilestone(milestone).subscribe(val => console.log(val));
  }
}
