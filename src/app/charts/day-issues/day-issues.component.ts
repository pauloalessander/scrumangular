import { Component, Input } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartType } from 'chart.js';
import { IssuesService } from 'src/services/issues/issues.service';

@Component({
  selector: 'app-day-issues',
  templateUrl: './day-issues.component.html',
  styleUrls: ['./day-issues.component.css']
})
export class DayIssuesComponent {
  
  @Input() milestone: string = "";

  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: [
      {
        data: [],
        label: 'Quant. Issues',
        fill: true,
        tension: 0.1,
        borderColor: 'white',
        backgroundColor: 'rgba(255,0,0,0.3)'
      }
    ]
  };

  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };

  constructor(private issueService: IssuesService) { }

  retrieveData(milestone: string) {
    this.issueService.getDays(milestone).subscribe(val => {
      this.lineChartData.datasets[0].data = this.issueService.getQuantIssuesFromDays(val)
      this.lineChartData.labels = this.issueService.getCreatedAtFromDays(val)
    });
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }
}