import { Component, Input } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartType } from 'chart.js';
import { IssuesService } from 'src/services/issues/issues.service';

@Component({
  selector: 'app-status-issues',
  templateUrl: './status-issues.component.html',
  styleUrls: ['./status-issues.component.css']
})
export class StatusIssuesComponent {

  @Input() milestone: string = "";

  public pieChartData: ChartConfiguration<'pie'>['data'] = {
    labels: [],
    datasets: [{
      data: [],
      backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
        '#008000'
      ],
      hoverBackgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
        '#008000'
      ]
    }]
  };

  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = []; 
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  
  constructor(private issueService: IssuesService) { }

  retrieveData(milestone: string) {
    this.issueService.getStatus(milestone).subscribe(val => {
      this.pieChartData.datasets[0].data = this.issueService.getQuantIssuesFromStatus(val)
      this.pieChartData.labels = this.issueService.getStatusFromStatus(val)
    });
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }
}