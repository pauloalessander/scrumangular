import { Component, Input } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartType } from 'chart.js';
import { IssuesService } from 'src/services/issues/issues.service';

@Component({
  selector: 'app-quant-issues-chart',
  templateUrl: './quant-issues-chart.component.html',
  styleUrls: ['./quant-issues-chart.component.css']
})
export class QuantIssuesChartComponent {

  @Input() milestone: string = "";

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [],
    datasets: [{
      label: 'Issues',
      data: [],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 99, 132)'
    }]
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  
  constructor(private issueService: IssuesService) {}

  retrieveData(milestone: string) {
    this.issueService.getQuant(milestone).subscribe(val => {
      this.barChartData.datasets[0].data = this.issueService.getQuantIssuesFromQuant(val)
      this.barChartData.labels = this.issueService.getSponsorsFromQuant(val)
    });
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }
}
