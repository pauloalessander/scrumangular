import { Component, Input } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartType } from 'chart.js';
import { MergesService } from 'src/services/merges/merges.service';

@Component({
  selector: 'app-quant-merges',
  templateUrl: './quant-merges.component.html',
  styleUrls: ['./quant-merges.component.css']
})
export class QuantMergesComponent {

  @Input() milestone: string = "";

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [],
    datasets: [{
      label: 'Merge Requests',
      data: [],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 99, 132)'
    }]
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  
  constructor(private mergeService: MergesService) {}

  retrieveData(milestone: string) {
    this.mergeService.getMerged(milestone).subscribe(val => {
      this.barChartData.datasets[0].data = this.mergeService.getQuantMergesFromDevMerged(val)
      this.barChartData.labels = this.mergeService.getSponsorFromDevMerged(val)
    });
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }
}
