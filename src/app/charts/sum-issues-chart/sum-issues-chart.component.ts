import { Component, Input } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartType } from 'chart.js';
import { IssuesService } from 'src/services/issues/issues.service';

@Component({
  selector: 'app-sum-issues-chart',
  templateUrl: './sum-issues-chart.component.html',
  styleUrls: ['./sum-issues-chart.component.css']
})
export class SumIssuesChartComponent {
  @Input() milestone: string = "";

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [],
    datasets: [{
      label: 'Pontos',
      data: [],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 99, 132)'
    }]
  };

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  
  constructor(private issueService: IssuesService) {}

  retrieveData(milestone: string) {
    this.issueService.getSums(milestone).subscribe(val => {
      this.barChartData.datasets[0].data = this.issueService.getSumIssuesFromSums(val)
      this.barChartData.labels = this.issueService.getSponsorsFromSums(val)
    });
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }
}
