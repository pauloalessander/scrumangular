import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Merge } from '../../services/merges/merge';
import { MergesService } from '../../services/merges/merges.service';
import { MergesTableDataSource } from './merges-table-datasource';

@Component({
  selector: 'app-merges-table',
  templateUrl: './merges-table.component.html',
  styleUrls: ['./merges-table.component.css']
})
export class MergesTableComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Merge>;
  dataSource!: MergesTableDataSource;
  dataLength!: number;
  errorMessage!: string;
  @Input() milestone: string = "";

  displayedColumns = [
    "cod",
    "issueCod",
    "state",
    "mergedby",
    "mergedat",
    "targetbranch",
    "milestone",
    "link"
  ];
  total!: number;
  data!: Merge[];

  constructor(private mergeService: MergesService) {}

  receiveMilestone(value: string) {
    this.milestone = value
    this.retrieveData(this.milestone);
  }
  
  retrieveData(milestone: string) {
    this.dataSource = new MergesTableDataSource(this.mergeService, milestone);
    this.mergeService.getMergeCount(milestone).subscribe({
      next: mergeCount => {
        this.dataLength = mergeCount;
      }, 
      error: err => {
        this.errorMessage = err;
      }
    });
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
