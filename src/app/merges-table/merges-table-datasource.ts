import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { mergeMap } from 'rxjs/operators';
import { Observable, of, merge } from 'rxjs';
import { Merge } from '../../services/merges/merge';
import { MergesService } from '../../services/merges/merges.service';
import { Inject } from '@angular/core';

export class MergesTableDataSource extends DataSource<Merge> {
  
  paginator!: MatPaginator;
  sort!: MatSort;

  constructor(private mergeService: MergesService, @Inject(String) private milestone: string) {
    super();
  }

  connect(): Observable<Merge[]> {

    return merge(of('Inicial load'), this.paginator.page, this.sort.sortChange, this.milestone)
            .pipe(mergeMap(() => {
              return this.mergeService.getMerges(
                this.paginator.pageIndex * this.paginator.pageSize,
                this.paginator.pageSize,
                this.sort.active,
                this.sort.direction,
                this.milestone
                );
    }));
  }

  disconnect(): void {}
}
