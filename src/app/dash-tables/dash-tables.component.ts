import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-dash-tables',
  templateUrl: './dash-tables.component.html',
  styleUrls: ['./dash-tables.component.css']
})
export class DashTablesComponent {
  cardLayout = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(() => {
     return {
        columns: 4,
        miniCard: { cols: 1, rows: 1 },
        minitable: { cols: 2, rows: 3 },
        chart: { cols: 4, rows: 3 },
        table: { cols: 4, rows: 4 }
      };
    })
  );

  milestone!: string;

  constructor(private breakpointObserver: BreakpointObserver) {}

  receiveMilestone(value: string) {  
    this.milestone = value
  }

}
