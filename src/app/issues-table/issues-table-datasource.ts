import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { mergeMap } from 'rxjs/operators';
import { Observable, merge, of } from 'rxjs';
import { Issue } from '../../services/issues/issue';
import { IssuesService } from '../../services/issues/issues.service';
import { Inject } from '@angular/core';


export class IssuesTableDataSource extends DataSource<Issue> {
  
  paginator!: MatPaginator;
  sort!: MatSort;

  constructor(private issueService: IssuesService, @Inject(String) private milestone: string) {
    super();
  }

  connect(): Observable<Issue[]> {

    return merge(of('Inicial load'), this.paginator.page, this.sort.sortChange, this.milestone)
            .pipe(mergeMap(() => {
              return this.issueService.getIssues(
                this.paginator.pageIndex * this.paginator.pageSize,
                this.paginator.pageSize,
                this.sort.active,
                this.sort.direction,
                this.milestone
                );
    }));
  }

  disconnect(): void {}
}
