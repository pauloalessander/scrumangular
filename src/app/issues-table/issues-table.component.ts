import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Issue } from '../../services/issues/issue';
import { IssuesService } from '../../services/issues/issues.service';
import { IssuesTableDataSource } from './issues-table-datasource';

@Component({
  selector: 'app-issues-table',
  templateUrl: './issues-table.component.html',
  styleUrls: ['./issues-table.component.css']
})
export class IssuesTableComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Issue>;
  dataSource!: IssuesTableDataSource;
  dataLength!: number;
  errorMessage!: string;
  @Input() milestone: string = "";

  displayedColumns = [
    "cod",
    "title",
    "createdat",
    "author",
    "sponsor",
    "link",
    "milestone",
    "time",
    "timedesc",
    "planning",
    "status",
    "project",
    "point",
    "isplan"
  ];
  total!: number;
  data!: Issue[];
  veio!: number[];

  constructor(private issueService: IssuesService) {}

  receiveMilestone(value: string) {  
    this.milestone = value
    this.retrieveData(this.milestone);
  }

  retrieveData(milestone: string) {
    this.dataSource = new IssuesTableDataSource(this.issueService, milestone);
    this.issueService.getIssueCount(milestone).subscribe({
      next: issueCount => {
        this.dataLength = issueCount;
      }, 
      error: err => {
        this.errorMessage = err;
      }
    });
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  ngOnChanges() {
    this.retrieveData(this.milestone);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
